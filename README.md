# Introduction

Library for generating different Lorem Ipsum texts for backend / frontend usage. 

# How to

## build

npm run build

## run tests

npm run test

# References

## Fast-lorem-ipsum lib

https://www.npmjs.com/package/fast-lorem-ipsum 

## NPM Module

Used next material to achieve NPM Module for node and browsers- https://www.made-on-mars.com/blog/how-to-create-a-npm-module-that-works-on-browser-with-webpack-2/

# License

MIT
