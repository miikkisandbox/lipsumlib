const getLipsumText = require('../lib/lipsumlib');

expect.extend({
  toHaveNWords(received, argument) {
    const splitString = received.split(" ");
    const pass = splitString.length % argument == 0;
    if (pass) {
      return {
        message: () =>
          `expected ${received} not to have ${argument} words in it`,
        pass: true,
      };
    } else {
      return {
        message: () => `expected ${received} to have ${argument} words in it`,
        pass: false,
      };
    }
  },
});


// Successful with capital letters
test('should return 5 words with parameters 5 and W', () => {  
  expect(getLipsumText(5, "W")).toHaveNWords(5);
});

test('should return 5 characters with parameters 5 and C', () => {  
  expect(getLipsumText(5, "c")).toHaveLength(5);
});

// Successful with lower case letters
test('should return 5 words with parameters 5 and w', () => {  
  expect(getLipsumText(5, "w")).toHaveNWords(5);
});

test('should return 5 characters with parameters 5 and c', () => {  
  expect(getLipsumText(5, "c")).toHaveLength(5);
});