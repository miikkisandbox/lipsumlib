var fli = require("fast-lorem-ipsum");

/**
 * Get Lorem Ipsum Text
 * 
 * @param {number} count 
 * @param {string} type W for words, C for chars
 * @returns {string} Lorem Ipsum text
 */
const getLipsumText = (count, type) => {  
  return fli(count, type.toLowerCase());
};

module.exports = getLipsumText;